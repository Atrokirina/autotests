const path = require('path');

module.exports = require(`../../../common/${path.basename(__filename)}`)(
  path
    .dirname(__filename)
    .split(path.sep)
    .reverse()[1]
);
