module.exports = {
  disabled: false, // Флаг отключения тестирования.
  sitemapExpTime: 86400, // Время протухания карты сайта в секундах. По-умолчанию - 1 сутки.
  url: ['https://google.com', 'http://yandex.ru'], // Урлы проекта. Используется для формирования карты сайта. Нужно указать как поддомены.
  respectRobotsTxt: true, // Карта сайта строится согласно robots.txt.
  tests: [], // Список тестов для тестирования, пример: inline_styles.js.
  needsAuth: false, // Если нужна http-авторизация. Например, для беты.
  username: 'beta', // ...
  password: 'beta' // ...
};
