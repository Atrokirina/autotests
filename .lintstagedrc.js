module.exports = {
  linters: {
    '*.(js|jsx)': ['npm run lint:write', 'prettier --write', 'git add']
  },
  relative: true
}
