module.exports = {
  disabled: false,
  name: 'Битые ссылки',
  components: ['publishing'],
  subtitle: ['Битые ссылки'],
  fn: function(browser, data) {
    let errorUrls = [];
    let urls404 = data.sitemapObj.get404Urls();
    urls404.forEach((item, key) => {
      browser.elements('css selector', 'a[href="' + item + '"]', function(result) {
        if (result.value.length > 0) errorUrls.push(item);

        if (key + 1 === urls404.length)
          browser.verify.ok(errorUrls.length === 0, 'Битые ссылки{{{' + Array.from(new Set(errorUrls)).join(', '));
      });
    });
  }
};
