module.exports = {
  disabled: false,
  name: 'Название страницы',
  components: ['publishing'],
  subtitle: ['<title> пуст'],
  fn: function(browser) {
    browser.getTitle(title => {
      browser.verify.ok(title.trim().length !== 0, '<title> пуст');
    });
  }
};
