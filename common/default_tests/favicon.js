const parseString = require('xml2js').parseString;
const _ = require('lodash');
const axios = require('axios');

//кэш имен обработанных файлов
let processedFiles = [];

let trimLeft = function(str, charlist) {
  if (charlist === undefined) charlist = 's';
  return str.replace(new RegExp('^[' + charlist + ']+'), '');
};

function checkFile(browser, file, options = {}) {
  return new Promise(function(resolve) {
    //если уже проверяли, не делаем
    if (processedFiles.includes(file)) {
      resolve({ status: false });
      return;
    }
    processedFiles.push(file);
    axios
      .get(file, { timeout: 10000 })
      .then(function(response) {
        resolve({ status: true, body: response.data });
        return;
      })
      .catch(function(err) {
        if (err.code === 'ECONNABORTED') {
          browser.verify.ok(
            false,
            'Превышено время ожидания файла' + '{{{' + file + (options.file ? ' в ' + options.file : '')
          );
          resolve({ status: false });
          return;
        }
        browser.verify.ok(
          false,
          (options.message ? options.message : 'Битая ссылка на файл') +
            '{{{' +
            file +
            (options.file ? ' в ' + options.file : '')
        );
        resolve({ status: false });
        return;
      });
  });
}

function checkMSconfig(browser, siteRoot, msConfigfile) {
  return new Promise(function(resolve) {
    checkFile(browser, siteRoot + trimLeft(msConfigfile, '/'), { message: 'Отсутствует файл' }).then(result => {
      //Файла нет или уже была проверка
      if (!result.status) {
        resolve();
        return;
      }

      parseString(result.body, function(err, result) {
        if (err) {
          browser.verify.ok(false, 'Неверный формат browserconfig.xml');
          resolve();
          return;
        }

        let tile = _.get(result, 'browserconfig.msapplication[0].tile[0]');
        if (!tile || typeof tile !== 'object') {
          browser.verify.ok(false, 'Неверный формат browserconfig.xml');
          resolve();
          return;
        }

        let possibleIconTags = [
          'square70x70logo',
          'square150x150logo',
          'wide310x150logo',
          'square310x310logo',
          'TileImage'
        ];
        let promises = [];
        for (const key of Object.keys(tile)) {
          if (possibleIconTags.includes(key)) {
            let iconFile = _.get(tile, [key, '0', '$', 'src']);
            if (!iconFile || '' === iconFile) {
              browser.verify.ok(false, 'Неверный формат browserconfig.xml');
            } else {
              promises.push(checkFile(browser, siteRoot + trimLeft(iconFile, '/'), { file: msConfigfile }));
            }
          }
        }
        Promise.all(promises).then(() => {
          resolve();
        });
      });
    }); //checkFile.then
  }); //promise
}

function checkAndroidManifest(browser, siteRoot, manifestFile) {
  return new Promise(resolve => {
    checkFile(browser, manifestFile).then(result => {
      //Файла нет или уже была проверка
      if (!result.status) {
        resolve();
        return;
      }
      let manifest = result.body;
      if (typeof manifest === 'string') {
        try {
          manifest = JSON.parse(manifest);
        } catch (e) {
          browser.verify.ok(false, 'Неверный формат site.webmanifest');
          resolve();
          return;
        }
      }
      if (!manifest || typeof manifest !== 'object') {
        browser.verify.ok(false, 'Неверный формат site.webmanifest');
        resolve();
        return;
      }
      let icons = _.get(manifest, 'icons');
      if (!icons) {
        browser.verify.ok(false, 'Неверный формат site.webmanifest');
        resolve();
        return;
      }
      //Проверка ссылок на иконки
      let promises = [];
      let has192 = false;
      icons.forEach(item => {
        let sizes = _.get(item, 'sizes');
        let src = _.get(item, 'src');
        if (sizes === '192x192') has192 = true;
        if (!src || '' === src) {
          browser.verify.ok(false, 'Неверный формат site.webmanifest');
          resolve();
          return;
        }
        let url = siteRoot + trimLeft(src, '/');
        if (!src.startsWith('/')) {
          //не от корня сайта, нужно искать относительно пути манифеста
          url = manifestFile.slice(0, manifestFile.lastIndexOf('/')) + '/' + src;
        }
        promises.push(checkFile(browser, url, { file: manifestFile }));
      });
      Promise.all(promises).then(() => {
        if (!has192) browser.verify.ok(false, 'Отсутствует icon для android 192x192{{{' + manifestFile);
        resolve();
      });
    }); //then
  }); //promise
}

function getMsConfigFileName(browser) {
  return new Promise(function(resolve) {
    browser.elements('css selector', 'meta[name="msapplication-config"]', function(result) {
      if (result.value.length > 0) {
        browser.getAttribute('meta[name="msapplication-config"]', 'content', function(result) {
          resolve(result.value);
          return;
        });
        //Если нет тега возвращаем имя по умолчанию
      } else {
        resolve('browserconfig.xml');
      }
    });
  });
}

function getAndroidManifestFileName(browser) {
  return new Promise(function(resolve) {
    browser.elements('css selector', 'link[rel~="manifest"][href]', function(result) {
      if (result.value.length > 0) {
        browser.getAttribute('link[rel~="manifest"][href]', 'href', function(result) {
          resolve(result.value);
          return;
        });
      } else {
        browser.verify.ok(false, 'Отсутствует web app manifest для android');
        resolve(false);
      }
    });
  });
}

module.exports = {
  disabled: false,
  name: 'favicon',
  components: ['webmaster'],
  subtitle: [
    'Отсутсвует favicon',
    'Отсутсвует favicon 16x16',
    'Отсутсвует favicon 32x32',
    'Отсутствует favicon для ios safari',
    'Отсутствует favicon для ios safari 180x180',
    'Отсутствует mask icon для safari',
    'Отсутствует цвет у mask icon для safari',
    'Отсутствует theme-color',
    'Отсутствует ms TileColor',
    'Отсутствует web app manifest для android',
    'Отсутствует icon для android 192x192',
    'Неверный формат site.webmanifest',
    'Неверный формат browserconfig.xml',
    'Отсутствует файл',
    'Битая ссылка на файл',
    'Превышено время ожидания файла'
  ],
  fn: function(browser, data) {
    let siteRoot = data.sitemapObj.getUrls()[0];

    //Таймаут на browser.perform 30 сек
    let oldTimeout = browser.globals.asyncHookTimeout;
    browser.globals.asyncHookTimeout = 30000;
    if (browser.currentTest.name === siteRoot) {
      processedFiles = [];
    }

    browser.elements('css selector', 'link[rel~="icon"]', function(result) {
      if (result.value.length === 0) browser.verify.ok(false, 'Отсутсвует favicon');
    });
    browser.elements('css selector', 'link[rel~="icon"][sizes="16x16"]', function(result) {
      if (result.value.length === 0) browser.verify.ok(false, 'Отсутсвует favicon 16x16');
    });
    browser.elements('css selector', 'link[rel~="icon"][sizes="32x32"]', function(result) {
      if (result.value.length === 0) browser.verify.ok(false, 'Отсутсвует favicon 32x32');
    });

    browser.elements('css selector', 'link[rel~="apple-touch-icon"][href]', function(result) {
      if (result.value.length === 0) browser.verify.ok(false, 'Отсутствует favicon для ios safari');
    });
    browser.elements('css selector', 'link[rel~="apple-touch-icon"][sizes="180x180"][href]', function(result) {
      if (result.value.length === 0) browser.verify.ok(false, 'Отсутствует favicon для ios safari 180x180');
    });

    browser.elements('css selector', 'link[rel~="mask-icon"][href]', function(result) {
      if (result.value.length === 0) browser.verify.ok(false, 'Отсутствует mask icon для safari');
    });
    browser.elements('css selector', 'link[rel~="mask-icon"][color]', function(result) {
      if (result.value.length === 0) browser.verify.ok(false, 'Отсутствует цвет у mask icon для safari');
    });

    browser.elements('css selector', 'meta[name="theme-color"][content]', function(result) {
      if (result.value.length === 0) browser.verify.ok(false, 'Отсутствует theme-color');
    });
    browser.elements('css selector', 'meta[name="msapplication-TileColor"][content]', function(result) {
      if (result.value.length === 0) browser.verify.ok(false, 'Отсутствует ms TileColor');
    });

    //Проверяем конфигурацию ms windows
    browser.perform(function(done) {
      getMsConfigFileName(browser)
        .then(file => {
          if (file) {
            return checkMSconfig(browser, siteRoot, file);
          }
        })
        .then(() => {
          done();
        });
    });
    //Проверяем манифест android
    browser.perform(function(done) {
      getAndroidManifestFileName(browser)
        .then(file => {
          if (file) {
            return checkAndroidManifest(browser, siteRoot, file);
          }
        })
        .then(() => {
          browser.globals.asyncHookTimeout = oldTimeout;
          done();
        });
    });
  }
};
