module.exports = {
  disabled: false,
  name: 'doctype',
  components: ['webmaster'],
  subtitle: [
    'Не соответствует формату <!DOCTYPE html>',
    'Отсутствует или есть некорректные символы перед <!DOCTYPE html>',
    'Есть некорректные символы перед <!DOCTYPE html>'
  ],
  fn: function(browser) {
    browser.execute(
      function() {
        var result = 0,
          node = document.doctype;

        if (node == null) {
          result = 2; // Отсутствует или есть некорректные символы перед <!DOCTYPE html>
        } else {
          var htmlDocType =
            '<!DOCTYPE ' +
            node.name +
            (node.publicId ? ' PUBLIC "' + node.publicId + '"' : '') +
            (!node.publicId && node.systemId ? ' SYSTEM' : '') +
            (node.systemId ? ' "' + node.systemId + '"' : '') +
            '>';

          if (htmlDocType != '<!DOCTYPE html>') {
            result = 1; // Не соответствует <!DOCTYPE html>
          } else {
            var dom = new XMLSerializer().serializeToString(document);
            if (dom.indexOf('<!DOCTYPE html>') !== 0) {
              result = 3; // Есть некорректные символы перед <!DOCTYPE html>
            }
          }
        }
        return result;
      },
      [],
      function(result) {
        if (result.value == 1) {
          browser.verify.ok(false, 'Не соответствует формату <!DOCTYPE html>');
        } else if (result.value == 2) {
          browser.verify.ok(false, 'Отсутствует или есть некорректные символы перед <!DOCTYPE html>');
        } else if (result.value == 3) {
          browser.verify.ok(false, 'Есть некорректные символы перед <!DOCTYPE html>');
        }
      }
    );
  }
};
