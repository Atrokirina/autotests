module.exports = {
  disabled: false,
  name: 'Инлайновые стили',
  components: ['publishing'],
  subtitle: ['Присутствует атрибут style'],
  fn: function(browser) {
    browser.element('xpath', './/*[not(descendant::noscript)]/*[@style]', function(result) {
      browser.verify.ok(!result.value, 'Присутствует атрибут style');
    });
  }
};
