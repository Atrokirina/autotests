const Test = require('../models/Test');

module.exports = function(projectName) {
  let TestObj = new Test(projectName);

  return TestObj.getExportObject();
};
