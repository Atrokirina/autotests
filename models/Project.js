module.exports = class Project {
  constructor(project) {
    this.project = project;
    this.setConfig();
  }

  setConfig() {
    const configPath = `../projects/${this.project}/config.js`;

    try {
      this.config = require(configPath);
    } catch (e) {
      throw new Error(`Config for ${this.project} not found`);
    }
  }

  getConfig() {
    return this.config;
  }

  getName() {
    return this.project;
  }

  isDisabled() {
    return this.config.disabled;
  }

  getType() {
    return this.config.type !== undefined ? this.config.type : 'regular';
  }

  canSendNotification() {
    return this.config.type !== 'custom' || this.config.type === undefined;
  }
};
