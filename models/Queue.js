const DB = require('./DB.js'),
  Project = require('./Project'),
  fs = require('fs');

module.exports = class Queue extends DB {
  constructor(type) {
    switch (type) {
      case 'regular':
        super('./db/queue.db');
        this.next = this.regular();
        break;
      case 'custom':
        super('./db/interface.db');
        this.next = this.custom();
        break;
    }
  }

  async custom() {
    await this.removeOldProjects(15);

    let unfinished = await this.find({ status: 'testing' });
    if (unfinished.length > 3) return false;
    let query = unfinished.length === 2 ? { extra: true } : {};

    let projects = [];
    let data = await this.find(query);
    data.forEach(item => {
      if (item.status === 'new') {
        projects.push({ name: item.project, date: item.dateAdd });
      }
      projects = projects.sort((a, b) => parseFloat(a.date) - parseFloat(b.date));
    });

    return projects[0] ? projects[0].name : false;
  }

  async regular() {
    let unfinished_projects = await this.find({ finished: false });
    if (unfinished_projects.length >= 2) return false;

    let finished = [];
    let unfinished = [];
    let projects = [];

    let fetchedData = await this.find({});

    fetchedData.forEach(item => {
      if (item.finished) finished[item.project] = item.dateFinish;
      else unfinished[item.project] = 0;
    });

    fs.readdirSync('./projects').forEach(project => {
      if (project === 'PROJECT_NAME') return;

      let projectObj;
      try {
        projectObj = new Project(project);
      } catch (err) {
        console.log(err);
        return;
      }
      if (!projectObj.isDisabled() && unfinished[project] === undefined && projectObj.canSendNotification())
        projects.push({ name: project, date: finished[project] ? finished[project] : 0 });
    });

    projects = projects.sort((a, b) => parseFloat(a.date) - parseFloat(b.date));

    return projects[0] ? projects[0].name : false;
  }

  async removeOldProjects(count) {
    const finished = await this.find({ status: 'finished' });
    if (finished.length > count) {
      let for_delete = [];
      finished.forEach(item => {
        for_delete.push({ name: item.project, date: item.dateFinish });
      });
      for_delete = for_delete.sort((a, b) => parseFloat(b.date) - parseFloat(a.date));

      let project_for_remove = for_delete[count].name;

      fs.rmdirSync(`./projects/${project_for_remove}`, { recursive: true });
      fs.rmdirSync(`./reports/${project_for_remove}`, { recursive: true });
      if (fs.existsSync(`./sitemaps/${project_for_remove}.json`))
        fs.unlinkSync(`./sitemaps/${project_for_remove}.json`);
      if (fs.existsSync(`./sitemaps/${project_for_remove}404.json`))
        fs.unlinkSync(`./sitemaps/${project_for_remove}404.json`);

      return await this.remove({ project: project_for_remove });
    }
    return false;
  }
};
