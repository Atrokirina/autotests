const fs = require('fs'),
  path = require('path'),
  Sitemap = require('./Sitemap'),
  Project = require('./Project'),
  Queue = require('./Queue');

module.exports = class Test {
  constructor(project) {
    this.project = project;
    this.ProjectObj = new Project(this.project);
    this.config = this.ProjectObj.getConfig();
    this.type = this.ProjectObj.getType();
  }

  isDisabled() {
    return this.ProjectObj.isDisabled();
  }

  getExportObject() {
    let exportObject = this.getHooks();

    let SitemapObj = new Sitemap(this.project);
    let urls = SitemapObj.getUrls();

    const srcPath = `${path.resolve(__dirname)}/../common/default_tests/`;
    const tests = fs.readdirSync(srcPath);

    let selected_tests;
    if (this.config.tests) {
      selected_tests = this.config.tests.length > 0 ? this.config.tests : tests;
    } else {
      selected_tests = tests;
    }
    let auth = this.config.needsAuth;
    let username = this.config.username;
    let password = this.config.password;

    const projectExtPath = `../projects/${this.project}/common_ext`;
    let projectExtTests = [];

    if (fs.existsSync(projectExtPath)) projectExtTests = fs.readdirSync(projectExtPath);

    urls.forEach(function(url) {
      if (auth === true) {
        let regular = url.match(/(.+):\/\/(.+)/) || [];
        url = `${regular[1]}://${username}:${password}@${regular[2]}`;
      }
      exportObject[url] = function(browser) {
        browser.url(url);

        tests.forEach(function(test) {
          let select = selected_tests.indexOf(test) !== -1;
          let srcTest = require(srcPath + test);
          if (srcTest.disabled === false && select) srcTest.fn(browser, { sitemapObj: SitemapObj });
        });

        projectExtTests.forEach(function(test) {
          let select = selected_tests.indexOf(test) !== -1;
          let srcTest = require(projectExtPath + '/' + test);
          if (srcTest.disabled === false && select) srcTest.fn(browser, { sitemapObj: SitemapObj });
        });
      };
    });

    return exportObject;
  }

  getHooks() {
    let QueueObj = new Queue(this.type);

    let project = this.project;

    return {
      before: function(browser, done) {
        QueueObj.update(
          { project: project },
          {
            $set: {
              dateStart: Date.now(),
              dateStartPrint: new Date().toLocaleString(),
              status: 'testing',
              finished: false
            }
          }
        ).then(() => {
          done();
        });
      },
      after: function(browser, done) {
        QueueObj.update(
          { project: project },
          {
            $set: {
              dateFinish: Date.now(),
              dateFinishPrint: new Date().toLocaleString(),
              status: 'finished',
              finished: true
            }
          }
        ).then(() => {
          done();
        });
      }
    };
  }

  start() {
    let QueueObj = new Queue(this.type);

    let project = this.project;

    return QueueObj.update(
      { project: project },
      {
        $set: {
          dateStart: Date.now(),
          dateStartPrint: new Date().toLocaleString(),
          status: 'testing',
          finished: false
        }
      },
      { upsert: true }
    );
  }

  finish(error = null) {
    let QueueObj = new Queue(this.type);

    let project = this.project;

    return QueueObj.update(
      { project: project },
      {
        $set: {
          dateFinish: Date.now(),
          dateFinishPrint: new Date().toLocaleString(),
          finished: true,
          status: 'finished',
          error: error
        }
      }
    );
  }
};
