const child_process = require('child_process'),
  fs = require('fs'),
  Test = require('./Test'),
  Queue = require('./Queue'),
  Sitemap = require('./Sitemap');

module.exports = class Autotest {
  async start(type) {
    const QueueObj = new Queue(type);

    const nextProject = await QueueObj.next;

    if (!nextProject) return;

    let TestObj = new Test(nextProject);

    if (TestObj.isDisabled()) {
      TestObj.finish();
      return;
    }

    let SitemapObj = new Sitemap(nextProject);
    if (!SitemapObj.isActual()) {
      try {
        child_process.execSync(`node sitemap ${nextProject}`, { stdio: 'inherit' });
      } catch (err) {
        TestObj.finish('Не удалось сгенерировать sitemap');
      }
    }

    try {
      if (!fs.existsSync(`./projects/${nextProject}/common`)) {
        fs.mkdirSync(`./projects/${nextProject}/common`);
      }

      if (!fs.existsSync(`./projects/${nextProject}/common/common.js`))
        fs.copyFileSync(`./projects/PROJECT_NAME/common/common.js`, `./projects/${nextProject}/common/common.js`);

      child_process.execSync(
        `node --max_old_space_size=8192 nightwatch_env/app projects/${nextProject}/common --config nightwatch_env/nightwatch.conf.js`,
        { stdio: 'inherit' }
      );
    } catch (err) {
      // console.log(err);
    }
  }
};
