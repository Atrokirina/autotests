const fs = require('fs'),
  path = require('path'),
  Project = require('./Project');
module.exports = class Sitemap {
  constructor(project) {
    this.project = project;
    this.path = `${path.resolve(__dirname)}/../sitemaps/${this.project}.json`;
    this.path404 = `${path.resolve(__dirname)}/../sitemaps/${this.project}404.json`;
    this.config = new Project(this.project).getConfig();
  }

  isActual() {
    if (fs.existsSync(this.path)) {
      let sitemapExpTime = this.config.sitemapExpTime * 1000 || 86400000;
      let sitemapStat = fs.statSync(this.path);

      return !(sitemapStat.birthtimeMs + sitemapExpTime <= Date.now());
    }

    return false;
  }

  getUrls() {
    if (!fs.existsSync(this.path)) throw new Error(`Sitemap for ${this.project} not exists`);

    return require(this.path);
  }

  get404Urls() {
    if (!fs.existsSync(this.path404)) throw new Error(`Sitemap404 for ${this.project} not exists`);

    let urls = require(this.path404).map(item => {
      try {
        return decodeURI(item);
      } catch (err) {
        console.log(err);
      }
    });
    let relativeUrls = urls.map(item => {
      let url = new URL(item);
      try {
        return decodeURI(url.pathname);
      } catch (err) {
        console.log(err);
      }
    });

    return urls.concat(relativeUrls);
  }
};
