const DB = require('./DB.js'),
  path = require('path'),
  fs = require('fs'),
  nodeJsZip = require('nodeJs-zip');

module.exports = {
  getRegularProjects: async function() {
    let projects = await this.getProjectsFromDB('queue.db');
    projects.forEach(function(item) {
      if (item.finished === false) item.testing = true;
      if (item.finished === true) {
        if (item.testing) delete item.testing;
        item.finish = true;
        item.workTimeMin = Math.round((item.dateFinish - item.dateStart) / 1000 / 60);
      }
    });
    projects.sort(this.compareValues('dateFinish', 'asc'));
    projects.sort(this.compareValues('dateStart', 'desc'));
    return projects;
  },
  getCustomProjects: async function() {
    let projects = await this.getProjectsFromDB('interface.db');
    projects.forEach(function(item) {
      if (!item.$$deleted) {
        if (item.status === 'new') item.new = true;
        if (item.status === 'testing') {
          if (item.new) delete item.new;
          item.testing = true;
        }
        if (item.status === 'finished') {
          if (item.testing) delete item.testing;
          item.finish = true;
          item.workTimeMin = Math.round((item.dateFinish - item.dateStart) / 1000 / 60);
        }
      }
    });
    projects.sort(this.compareValues('dateFinish', 'asc'));
    projects.sort(this.compareValues('dateStart', 'desc'));
    projects.sort(this.compareValues('dateAdd', 'desc'));
    return projects;
  },
  getEnabledTests: function() {
    let result = [];
    const srcPath = `${path.resolve(__dirname)}/../common/default_tests/`;
    const tests = fs.readdirSync(srcPath);
    tests.forEach(function(test) {
      let srcTest = require(srcPath + test);
      if (srcTest.disabled === false) result.push({ name: srcTest.name, filename: test });
    });
    return result;
  },
  showProjectReports: function(project) {
    let result = [];
    const srcPath = `${path.resolve(__dirname)}/../reports/${project}`;
    const reports = fs.readdirSync(srcPath);
    reports.forEach(function(report) {
      if (!(report === 'report-common.html' || report === `${project}_reports.zip`)) result.push(report);
    });
    return result;
  },
  getProjectsFromDB: async function(dbname) {
    const database = new DB(`./db/${dbname}`);
    return await database.find({});
  },
  compareValues: function(key, order = 'asc') {
    return function innerSort(a, b) {
      if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
        return 0;
      }

      const varA = typeof a[key] === 'string' ? a[key].toUpperCase() : a[key];
      const varB = typeof b[key] === 'string' ? b[key].toUpperCase() : b[key];

      let comparison = 0;
      if (varA > varB) {
        comparison = 1;
      } else if (varA < varB) {
        comparison = -1;
      }
      return order === 'desc' ? comparison * -1 : comparison;
    };
  },
  getZipForDownload: function(project) {
    console.log(project);
    let dir = path.join(__dirname, `/../reports/${project}/`);
    let filepath = path.join(dir, `/${project}_reports.zip`);
    if (!fs.existsSync(filepath)) {
      nodeJsZip.zip(dir, {
        name: `${project}_reports`,
        dir: dir,
        filter: false
      });
    }
    return filepath;
  },
  createConfig: function(config) {
    delete config.submit;
    config.type = 'custom';
    config.disabled = false;
    config.sitemapExpTime = 1;
    config.sitemapCount = config.sitemapCount.length > 0 ? config.sitemapCount : 0;
    config.tests = config.tests.includes('all') ? [] : config.tests[0];
    config.needsAuth = config.username.length > 3 ? true : false;
    config.respectRobotsTxt = config.respectRobotsTxt === '1' ? true : false;
    config.extra = config.sitemapCount.length <= 10 ? true : false;
    this.makeDirectoriesAndWrite(config);
  },
  generateProjectName: function(config) {
    let sourceUrl = config.match(/(.+):\/\/(.+)/) || [];
    let project = sourceUrl.length === 0 ? config.substring(0, 8) : sourceUrl[2].substring(0, 8);
    let hash = (Math.random() + 1).toString(36).substring(2, 6);
    return `${project}_${hash}`;
  },
  makeDirectoriesAndWrite: function(config) {
    const projectName = this.generateProjectName(config.url[0]);
    fs.mkdirSync(`./projects/${projectName}/common`, { recursive: true });
    fs.copyFileSync(`./projects/PROJECT_NAME/common/common.js`, `./projects/${projectName}/common/common.js`);
    fs.copyFileSync(`./projects/PROJECT_NAME/config.js`, `./projects/${projectName}/config.js`);
    fs.writeFileSync(
      `./projects/${projectName}/config.js`,
      `module.exports = ` + JSON.stringify(config, null, 2),
      'utf-8'
    );
    const dbProject = {
      project: projectName,
      status: 'new',
      dateAdd: Date.now(),
      dateAddPrint: new Date().toLocaleString(),
      finished: true,
      extra: config.extra,
      _id: `IN_${projectName}`
    };
    fs.writeFileSync(`./db/interface.db`, JSON.stringify(dbProject), { flag: 'a+' });
  }
};
