const Project = require('./Project'),
  path = require('path'),
  fs = require('fs');

module.exports = class Report {
  constructor(project, results) {
    this.project = project;
    this.results = results;
    this.tests_config = this.getConfig();
    this.ProjectObj = new Project(this.project);

    if (!fs.existsSync(`./reports/`)) fs.mkdirSync(`./reports/`);
    if (!fs.existsSync(`./reports/${project}`)) fs.mkdirSync(`./reports/${project}`);
  }

  getResults(url) {
    //Резултаты теста - последний false, например: standard_typographics#L140
    const completed = this.results.modules.common.completed;
    let results = {};
    Object.keys(completed).forEach(key => {
      let elem = completed[key].lastError.message.match(/{.+}/s);
      if (elem !== null && elem.length == 1) {
        results[key] = JSON.parse(elem);
      } else {
        results[key] = { error: ['error // произошла ошибка'] };
      }
    });
    let filtered = { [url]: {} }; // если значение есть
    Object.keys(results[url]).forEach(elem => {
      if (results[url][elem].length > 0) {
        filtered[url][elem] = results[url][elem];
      }
    });
    return filtered;
  }

  getConfig() {
    const fs = require('fs');
    const tests_names = fs.readdirSync(`${path.resolve(__dirname)}/../common/default_tests/`);
    let config = {
      reportNames: {},
      reportMessages: {},
      reportComponents: {},
      template: {}
    };
    tests_names.forEach(key => {
      let current_test = require(`${path.resolve(__dirname)}/../common/default_tests/${key}`);
      let name = key.replace('.js', '');
      config.reportNames[name] = current_test.name;
      if (Array.isArray(current_test.subtitle)) {
        let subtitle = {};
        current_test.subtitle.forEach(key => {
          subtitle[key] = current_test.name;
        });
        config.reportMessages = Object.assign(config.reportMessages, subtitle);
      }
      if (current_test.components) config.reportComponents[name] = current_test.components;

      if (current_test.template) config.template[current_test.name] = current_test.template;
    });
    return config;
  }

  resultsFormat() {
    const templates = this.tests_config.template;
    const reportNames = this.tests_config.reportNames;
    const reportMessages = this.tests_config.reportMessages;
    let results = {};

    Object.keys(reportNames).forEach(key => {
      let curr_template = 'default';
      templates[reportNames[key]] ? (curr_template = templates[reportNames[key]]) : curr_template;
      results[reportNames[key]] = { empty: true, parts: {}, template: curr_template };
    });

    Object.keys(reportMessages).forEach(key => {
      results[reportMessages[key]]['parts'][key] = [];
    });

    return results;
  }

  groupResults() {
    const reportMessages = this.tests_config.reportMessages;
    const completed = this.results.modules.common.completed;
    const results = this.resultsFormat();

    Object.keys(completed).forEach(key => {
      let index = 0;
      completed[key].assertions.forEach(item => {
        if (item.failure) {
          let message = item.message.split(') -')[0].split('(')[1];
          let links404 = [];
          if (message.split('{{{')[1]) {
            links404 = message.split('{{{')[1];
            message = message.split('{{{')[0];
          }
          if (reportMessages[message]) {
            let template = results[reportMessages[message]]['template'];
            switch (template) {
              case 'alt': {
                const test_results = this.getResults(key);

                let result = message + ': ' + [].concat.apply([], Object.values(test_results[key])[index]);
                result = result.replace(/\s{2,}/g, ' ');

                results[reportMessages[message]]['empty'] = false;
                if (results[reportMessages[message]]['parts'][key]) {
                  results[reportMessages[message]]['parts'][key].push({
                    url: result,
                    links404: links404
                  });
                } else {
                  results[reportMessages[message]]['parts'][key] = [{ url: result, links404: links404 }];
                }
                index++;
                break;
              }
              default:
                results[reportMessages[message]]['empty'] = false;
                results[reportMessages[message]]['parts'][message].push({
                  url: key,
                  links404: links404
                });
            }
          }
        }
      });
    });
    return results;
  }

  generate() {
    const pug = require('pug');
    const fs = require('fs');
    const results = this.groupResults();
    const reportNames = this.tests_config.reportNames;
    let keys = [];
    let current_pug;
    Object.keys(reportNames).forEach(key => {
      switch (results[reportNames[key]]['template']) {
        case 'alt':
          current_pug = '/common/templates/test_alt.pug';
          break;
        default:
          current_pug = '/common/templates/test.pug';
      }
      if (!results[reportNames[key]]['empty']) {
        try {
          let html = pug.renderFile(`${path.resolve(__dirname)}/../${current_pug}`, {
            testName: reportNames[key],
            testCode: key,
            results: results[reportNames[key]]['parts'],
            date: new Date().toString()
          });
          fs.writeFileSync(`${path.resolve(__dirname)}/../reports/${this.project}/${key}.html`, html);
          //if (this.ProjectObj.canSendNotification()) this.sendNotification(key);
          keys.push(key);
        } catch (err) {
          console.log(err.toString());
        }
      }
    });

    let html = pug.renderFile(`${path.resolve(__dirname)}/../common/templates/index.pug`, {
      project: this.project,
      tests: keys,
      reportNames: reportNames
    });
    fs.writeFileSync(`${path.resolve(__dirname)}/../reports/${this.project}/report-common.html`, html);
  }

  sendNotification(testName) {
    const request = require('request');
    const config = this.ProjectObj.getConfig();
    const project = this.project;

    let url = config.url;
    if (url instanceof Array) url = url[0];
    url = url.split('//')[1];
    request.post(
      'https://someurlwashere' + url,
      {
        json: {
          report_url: 'http://autotest..su/' + this.project + '/' + testName + '.html',
          test_name: this.tests_config.reportNames[testName],
          components: this.tests_config.reportComponents[testName]
        }
      },
      function() {
        console.log('Report ' + testName + ' for ' + project + ' sent');
      }
    );
  }
};
