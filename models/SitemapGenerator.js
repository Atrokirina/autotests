const fs = require('fs'),
  path = require('path'),
  Test = require('./Test');
module.exports = class SitemapGenerator {
  constructor(sitemapObj) {
    this.sitemapObj = sitemapObj;
    this.project = sitemapObj.project;
    this.config = sitemapObj.config;
  }

  async done(urls, jsonUrls, json404) {
    fs.writeFileSync(this.sitemapObj.path, JSON.stringify(jsonUrls), 'utf8', function() {});
    fs.writeFileSync(this.sitemapObj.path404, JSON.stringify(json404), 'utf8', function() {});
    for (let i = 0; i < urls.length; i++) {
      if (fs.existsSync(`${path.resolve(__dirname)}/../sitemaps/${this.project}${i}.xml`))
        fs.unlinkSync(`${path.resolve(__dirname)}/../sitemaps/${this.project}${i}.xml`);
    }
  }

  generate() {
    const SitemapGenerator = require('sitemap-generator');
    let TestObj = new Test(this.project);
    TestObj.start();

    let urls = this.config.url;
    if (!(urls instanceof Array)) urls = [urls];

    let needsAuth = this.config.needsAuth;
    if (needsAuth === undefined) needsAuth = false;

    let respectRobotsTxt = this.config.respectRobotsTxt;
    if (respectRobotsTxt === undefined) respectRobotsTxt = true;

    let jsonUrls = [];
    let json404 = [];
    let doneCount = 0;
    let urlsCount = 0;

    let type = this.config.sitemapType;
    let limit = this.config.sitemapCount;
    let username = this.config.username;
    let password = this.config.password;

    if (type === 'custom') {
      urls.forEach(url => {
        let protocol = url.match(/(.+):\/\/(.+)/) || [];
        if (protocol.length === 0) {
          jsonUrls.push(`https://${url}`);
        } else {
          jsonUrls.push(url);
        }
      });
      (async () => {
        await this.done(urls, jsonUrls, json404);
      })();
      return true;
    }

    urls.forEach((url, key) => {
      let generator = SitemapGenerator(url, {
        stripQuerystring: false,
        filepath: './sitemaps/' + this.project + key + '.xml',
        respectRobotsTxt: respectRobotsTxt,
        supportedMimeTypes: [new RegExp('text/html')],
        downloadUnsupported: false,
        ignoreWWWDomain: true,
        needsAuth: needsAuth,
        authUser: username,
        authPass: password
      });

      generator.on('error', error => {
        if (error.code === 404) json404.push(error.url);
      });

      generator.on('done', async () => {
        if (jsonUrls.length === 0) {
          TestObj.finish('Не удалось сгенерировать sitemap');
        }
        if (needsAuth === false) {
          let data = await this.getJson(this.project + key);
          jsonUrls = jsonUrls.concat(data);
        }
        doneCount++;
        if (doneCount === urls.length) {
          (async () => {
            await this.done(urls, jsonUrls, json404);
          })();
        }
      });

      generator.on('add', url => {
        urlsCount++;
        jsonUrls.push(url);
        if (urlsCount === limit && type === 'limited') {
          (async () => {
            await this.done(urls, jsonUrls, json404);
          })();
          generator.stop();
        }
        console.log('Url added to sitemap ' + url);
      });

      // start the crawler
      generator.start();
    });
  }

  async getJson(fileName) {
    const util = require('util'),
      xml2js = require('xml2js');

    const xml2jsPromise = util.promisify(xml2js.parseString);
    const sitemapPath = `./sitemaps/${fileName}.xml`;

    if (!fs.existsSync(sitemapPath)) {
      console.log(`Sitemap xml for ${this.project} ${fileName} not found`);
      return [];
    }

    let fileData = fs.readFileSync(sitemapPath);
    let urls = await xml2jsPromise(fileData);

    let urlsList = [];
    for (let key in urls.urlset.url) {
      let url = urls.urlset.url[key].loc[0];
      let urlArray = url.split('/');
      let urlTail = urlArray[urlArray.length - 1];
      let typeArray = ['.html', '.htm', '.php'];
      let typeRegExp = new RegExp(typeArray.join('|'));
      if (urlTail.length == 0 || typeRegExp.test(urlTail) || urlTail.indexOf('.') < 0) {
        urlsList.push(url);
      }
    }

    return urlsList;
  }
};
