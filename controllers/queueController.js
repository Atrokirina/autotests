const Interface = require('../models/Interface'),
  path = require('path'),
  { body, validationResult } = require('express-validator');

module.exports.showRegular = async function(req, res) {
  const results = await Interface.getRegularProjects();
  res.render('regular', { projects: results, title: 'интерфейс' });
};

module.exports.showCustom = async function(req, res) {
  const results = await Interface.getCustomProjects();
  const tests = Interface.getEnabledTests();
  res.render('custom', { projects: results, tests: tests, title: 'интерфейс' });
};

module.exports.showProjectReports = function(req, res) {
  let project = req.params.code;
  res.sendFile(path.resolve(`reports/${project}/report-common.html`));
};

module.exports.showReport = function(req, res) {
  let project = req.params.code;
  let report = req.params.report;
  res.sendFile(path.resolve(`reports/${project}/${report}`));
};

module.exports.getReports = function(req, res) {
  let project = req.params.code;
  let filepath = Interface.getZipForDownload(project);
  res.download(filepath);
};

module.exports.addProjectCustom = [
  (req, res, next) => {
    req.body.url = req.body.url.split(/\r?\n/);
    req.body.tests = new Array(req.body.tests);
    next();
  },
  body('url')
    .notEmpty()
    .withMessage('Задано меньше одного URL'),
  body('sitemapType')
    .trim()
    .escape()
    .isIn(['default', 'custom'])
    .withMessage('Неверная конфигурация')
    .if(body('sitemapCount').notEmpty())
    .isIn(['default'])
    .withMessage('Несовместимые условия'),
  body('respectRobotsTxt')
    .trim()
    .escape(),
  body('sitemapCount')
    .trim()
    .escape()
    .if(body('sitemapCount').notEmpty())
    .isNumeric()
    .withMessage('Значение должно быть целым числом'),
  body('tests[].*')
    .escape()
    .isArray({ min: 1 })
    .withMessage('Неверная конфигурация'),
  body('username')
    .trim()
    .escape()
    .if(body('isauth').exists())
    .isLength({ min: 3 })
    .withMessage('Логин меньше 3 символов'),
  body('password')
    .trim()
    .escape()
    .if(body('isauth').exists())
    .isLength({ min: 3 })
    .withMessage('Пароль меньше 3 символов'),

  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const results = await Interface.getCustomProjects();
      const tests = Interface.getEnabledTests();
      res.render('custom', { projects: results, tests: tests, errors: errors.mapped(), title: 'Есть ошибки' });
    } else {
      Interface.createConfig(req.body);
      res.redirect('./');
    }
  }
];
//(?<=dateStartPrint":")([^(]+)([^"]+) to $1     Замена даты в базе данных для нормального отображения
//
// \sGMT\+0300\s to nothing
