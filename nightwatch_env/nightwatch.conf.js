const path = require('path');

module.exports = {
  globals_path: `${path.resolve(__dirname)}/globals.js`,
  webdriver: {
    start_process: false
  },
  live_output: false,

  test_settings: {
    default: {
      selenium_port: 4444,
      selenium_host: 'localhost',
      skip_testcases_on_fail: false,
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true,
        acceptSslCerts: true,
        chromeOptions: {
          args: ['--verbose', '--no-sandbox', '--headless', '--disable-gpu'],
          w3c: false
        }
      }
    }
  }
};
