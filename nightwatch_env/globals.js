const Report = require(`../models/Report`),
  projectName = process.argv[2].split('/')[1];

module.exports = {
  reporter: function(results, cb) {
    if (results.failed > 0 || results.errors > 0) new Report(projectName, results).generate();

    cb();
  }
};
