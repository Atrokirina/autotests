$(document).ready(function() {
  document.querySelector('textarea').addEventListener('input', function(e) {
    e.target.style.height = 'auto';
    e.target.style.height = e.target.scrollHeight + 2 + 'px';
  });
  $(function() {
    $('.badge-help').tooltip({
      template:
        '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner" style="max-width:350px;"></div></div>',
      title:
        '<b>Весь сайт:</b><br>Если нужно протестировать весь сайт, ничего не меняем.<br>' +
        '<b>С ограничением:</b><br>' +
        'Если нужно простестировать только несколько первых Url, то выставляем ограничение.<br>' +
        'Если тестируем отдельные страницы/страницу, то выбираем вариант <b>Только указанные Url.</b><br>' +
        '<b>*</b> В случае, когда введено несколько Url и выбран первый вариант, то одновременно в сайтмап добавляются адреса, полученные из обработки всех введенных Url (доменов).',
      placement: 'right',
      html: true,
      offset: '10'
    });
    $('.badge-sitemap').tooltip({
      template:
        '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner" style="max-width:350px;"></div></div>',
      title: 'Если среди выбранных есть опция <b>Использовать все доступные</b>, то использоваться будут все тесты.',
      placement: 'right',
      html: true,
      offset: '10'
    });
    $('.badge-add').tooltip({
      template:
        '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner" style="max-width:350px;"></div></div>',
      title:
        'Запуск следующего проекта из очереди осуществляется раз в <b>одну</b> минуту.<br>' +
        'Бейдж <b>E</b> добавляется к проекту если изначально выбрано ограничение до <b>10</b> Url или введено не более <b>10</b> Url. ' +
        'Данный бейдж позволяет быстрым проектам выполняться в случае наличия в очереди «тяжелых» проектов.<br>' +
        'После тестирования возможен просмотр и скачивание отчетов, а также просмотр ошибок, при наличии.<br>' +
        '<b>Важно! Сохраняются только последние 15 проектов. При необходимости скачивайте отчеты сразу!</b>',
      placement: 'top',
      html: true,
      offset: '10'
    });
  });
});
