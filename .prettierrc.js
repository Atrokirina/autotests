module.exports = {
  semi: true,
  printWidth: 120,
  singleQuote: true,
  bracketSpacing: true,
  jsxBracketSameLine: true
};
