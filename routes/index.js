var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.redirect('/queue/regular/');
});

router.get('/interface.php', function(req, res) {
  res.redirect('/queue/custom/');
});

router.get('/queue.php', function(req, res) {
  res.redirect('/queue/regular/');
});

module.exports = router;
