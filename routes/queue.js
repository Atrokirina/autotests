const express = require('express'),
  router = express.Router(),
  queueController = require('../controllers/queueController');

router.get('/regular/', queueController.showRegular);
router.get('/regular/:code/', queueController.showProjectReports);
router.get('/regular/:code/:report/', queueController.showReport);

router.get('/custom/', queueController.showCustom);
router.post('/custom/', queueController.addProjectCustom);
router.get('/custom/:code/', queueController.showProjectReports);
router.get('/custom/:code/download/', queueController.getReports);
router.get('/custom/:code/:report/', queueController.showReport);

module.exports = router;
