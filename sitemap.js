const Sitemap = require('./models/Sitemap'),
  SitemapGenerator = require('./models/SitemapGenerator');
const args = process.argv.slice(2)[0];

if (args.length > 0) {
  try {
    const SitemapObj = new Sitemap(args);
    const SitemapGeneratorObj = new SitemapGenerator(SitemapObj);
    SitemapGeneratorObj.generate();
  } catch (e) {
    console.log(e);
  }
}
